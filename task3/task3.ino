#define CLOCK_PIN 2
#define RESET_PIN 3

String number_to_show = "";
long prev = 0;
int pos = 0;

void resetNumber()
{
  digitalWrite(RESET_PIN, HIGH);
  digitalWrite(RESET_PIN, LOW);
}

void showNumber(int n)
{
  resetNumber();

  for (byte i=0; i < n; i++) {
    digitalWrite(CLOCK_PIN, HIGH);
    digitalWrite(CLOCK_PIN, LOW);
  }
}

void setup(){
  pinMode(RESET_PIN, OUTPUT);
  pinMode(CLOCK_PIN, OUTPUT);
  resetNumber();
  Serial.begin(9600);
}

void loop(){
  if (Serial.available()){
    number_to_show = "";
    pos = 0;
    while (Serial.available()){
      number_to_show += char(Serial.read());
      delay(50);
    }
  }
  while (millis() - prev >= 1000){
    prev = millis();
    if (pos == 0){
      pos++;
      if (number_to_show.length() > 2){
        resetNumber();
        break;
      }
    }
    int show = (number_to_show[pos - 1] - '0') * 10 + number_to_show[pos] - '0';
    showNumber(show);
    pos++;
    pos %= number_to_show.length();
  }
}